import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Article } from '../interfaces/article';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
//import { ARTICLES } from '../interfaces/mock-articles';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  urlArticles = environment.apiUrl + '/articles';


  constructor(private httpClient: HttpClient) { }

  getArticles(): Observable<Article[]> {
    /* code with Mock-Articles:
    const articles: Article[] = ARTICLES;
    return of(articles);
    */

    return this.httpClient.get<Article[]>(this.urlArticles);
  }

  getArticle(key: string): Observable<Article> {
    /* code with Mock-Articles
    const articles: Article[] = ARTICLES.filter(article => article.key === key);
    return of(articles[0]);
    */
    return this.httpClient.get<Article>(this.urlArticles + '/' + key);
  }
}
