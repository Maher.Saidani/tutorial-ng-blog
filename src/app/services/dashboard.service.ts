import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../interfaces/article';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpClient: HttpClient) { }

  getArticles(): Observable<Article[]>{
    return this.httpClient.get<Article[]>(environment.apiUrl + '/dashboard/overview');
  }

  togglePublishState(article: Article): Observable<Article>{
    return this.httpClient.post<Article>(environment.apiUrl + '/dashboard/articles/publish', article);
  }

  getArticle(key: string): Observable<Article>{
    return this.httpClient.get<Article>(environment.apiUrl + '/dashboard/article/' + key);
  }

  updateArticle(article: Article): Observable<Article>{
    return this.httpClient.put<Article>(environment.apiUrl + '/dashboard/article', article);
  }

  deleteArticle(id: number): Observable<any>{
    return this.httpClient.delete<any>(environment.apiUrl + '/dashboard/article/' + id);
  }

  createArticle(article: Article): Observable<Article>{
    return this.httpClient.post<Article>(environment.apiUrl + '/dashboard/article', article);
  }

}
