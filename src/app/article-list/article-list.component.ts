import { Component, OnInit } from '@angular/core';
import { Article } from '../interfaces/article';
import { ArticleService } from '../services/article.service';
import { Title } from '@angular/platform-browser';
import { SharedService } from '../services/shared.service';
//import { ARTICLES } from '../interfaces/mock-articles';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  title = 'Articles';
  articles: Article[];

  constructor(
    private articleService: ArticleService,
    private titleService: Title,
    private sharedService: SharedService) {
  }

  ngOnInit() {
    this.getArticles();
    this.titleService.setTitle(`${this.title} - ${this.sharedService.blogTitle}`);
  }

  getArticles(): void {
    this.articleService.getArticles().subscribe(
      articles => this.articles = articles
    );
  }

}
