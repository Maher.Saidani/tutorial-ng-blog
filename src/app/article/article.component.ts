import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from '../interfaces/article';
import { ArticleService } from '../services/article.service';
import {Router} from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { SharedService } from '../services/shared.service';
import { Observable } from 'rxjs';
import { DashboardService } from '../services/dashboard.service';




@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  article: Article = {key: 'key'};

  constructor(
    private root: ActivatedRoute,
    private articleService: ArticleService,
    private router: Router,
    private titleService: Title,
    private sharedService: SharedService,
    private metaService: Meta,
    private dashboardService: DashboardService) {}

  ngOnInit() {
    this.root.params.subscribe(
      params => {
        if(this.router.url.indexOf("dashboard/preview") === -1){
          this.articleService.getArticle(params.key).subscribe(article => this.displayArticle(article));
        } else {
          this.dashboardService.getArticle(params.key).subscribe(article => this.displayArticle(article));
        }
      }
    );
  }

  setMeta() {
    this.metaService.addTags([
      { name: 'description', content: this.article.description },
      { property: 'og:title', content: `${this.article.title} - ${this.sharedService.blogTitle}` },
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: this.sharedService.baseUrl + this.article.key },
      { property: 'og:image', content: this.article.imageUrl },
      { property: 'og:description', content: this.article.description },
      { property: 'og:siteNae', content: this.sharedService.blogTitle }
    ]);
  }

  displayArticle(article: Article): void {
    if (article === null) {
      this.router.navigate(['notfound']);
      this.article.key = 'undefined key';
    } else {
      this.article = article;
      this.titleService.setTitle(`${this.article.title} - ${this.sharedService.blogTitle}`);
      this.setMeta();
    }
  }
}
