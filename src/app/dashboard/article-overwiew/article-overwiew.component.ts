import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/article';
import { DashboardService } from 'src/app/services/dashboard.service';



@Component({
  selector: 'app-article-overwiew',
  templateUrl: './article-overwiew.component.html',
  styleUrls: ['./article-overwiew.component.css']
})
export class ArticleOverwiewComponent implements OnInit {

  articles: Article[];

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getArticles();
  }

  getArticles(): void {
    this.dashboardService.getArticles().subscribe(
      articles => this.articles = articles
    );
  }

  togglePublishState(article: Article): void {
    article.published = !article.published;
    this.dashboardService.togglePublishState(article).subscribe(
      result => {
        const index: number = this.articles.findIndex(
          currentArticle => result.id === currentArticle.id
        );
        this.articles[index] = result;
      },
      error => {
        article.published = !article.published;
        console.error(error);
      }
    );
  }

}
