import { Component, OnInit, RootRenderer } from '@angular/core';
import { Article } from 'src/app/interfaces/article';
import { DashboardService } from 'src/app/services/dashboard.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {

  article: Article = {key: 'key'};
  isSaved: boolean = false;
  isNew: boolean = false;
  

  constructor(
    private dashboardService: DashboardService,
    private router: Router,
    private route: ActivatedRoute
    ) {}

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        const key: string = params.key;
        if(key === 'new'){
          this.article = {key: ' ', date: new Date(), published: false};
          this.isNew = true;
        } else {
          this.gettAricle(key);
        }
      }
    );
  }

  gettAricle(key: string): void {
    this.dashboardService.getArticle(key).subscribe(
      (article: Article) => {
        if (article === null){
          console.log('article not fount');
          this.router.navigateByUrl('notfound');
          return;
        }
        this.article = article;
      }
    );
  }

  updateArticle(): void{
    this.isSaved = false;
    console.log(this.isSaved);
    this.dashboardService.updateArticle(this.article).subscribe(
      //Innere funktion wird nicht unbedingt gebraucht
      article => {
        this.article = article;
        this.isSaved = true;
        console.log(article);
      }
    );
  }

  viewPreview(): void{
    this.router.navigateByUrl('/dashboard/preview/' + this.article.key);
  }

  deleteArticle(): void{
    this.isSaved = false;
    const confirmDelete = confirm(`Are you sure to delete '${this.article.title}' ?`);
    if(confirmDelete){
      this.dashboardService.deleteArticle(this.article.id).subscribe(
        () => { 
          this.router.navigateByUrl('dashboard'); 
        },
        error => { alert(error.error.message); }
      );
    }
  }

  createArticle(): void{
    this.dashboardService.createArticle(this.article).subscribe(
      (result) => {
          this.article = result;
          this.isSaved = true;
          this.isNew = false;
      }
    );
  }

  updateKey(): void{
    this.article.key = this.article.title.toLocaleLowerCase().replace(new RegExp(' ','g'),'-');
  }

}
